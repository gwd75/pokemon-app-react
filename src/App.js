import { useState, useEffect } from "react";
import "./App.scss";
import "./PokemonColors.scss"
import PokemonCard from "./Components/PokemonCard/PokemonCard";
import api from "./api";

function App() {
  const [pokemonMaxId, setPokemonMaxId] = useState(649);
  const [pokemonData, setPokemonData] = useState([]);

  const fetchData = async () => {
    let newData = [];
    for (let i = 1; i < pokemonMaxId + 1; i++) {
      console.log(i);
      const res = await api.get("https://pokeapi.co/api/v2/pokemon/" + i)
      newData.push(res.data)
    }
    // console.log(newData);
    // console.log(newData.length);
    setPokemonData(newData);
  };

  const fetchSpecificData = async (e) => {
    e.preventDefault()
    console.log(e.target.value.length);
    if(e.target.value.length === 0){
      fetchData()
    } else {
      let newData = []
      const res = await api.get(`https://pokeapi.co/api/v2/pokemon/${e.target.value}`)
      newData.push(res.data)
      setPokemonData(newData)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  // console.log(pokemonData.length);

  return (
    <div className="App">
      <h1>Pokémon !</h1>
      <form onChange={fetchSpecificData}>
        <input type="search" id="search-pokemon" name="search-pokemon" placeholder="rechercher un pokémon" />
        {/* <input type="submit"/> */}
      </form>
      <div className="pokemon-container">
        {/* {pokemonData.length === 0 ? fetchData() : fetchData()} */}
        {pokemonData.map((item, index) => {
          return <PokemonCard pok={item} key={index} />;
        })}
      </div>
    </div>
  );
}

export default App;
