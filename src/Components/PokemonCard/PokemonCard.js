import React, { useState } from "react";
import PokemonModal from "../PokemonModal/PokemonModal";
import "./PokemonCard.scss";

export default function PokemonCard({ pok }) {
  const [showModal, setShowModal] = useState(false);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  const pokemonMainType = pok.types[0].type.name;
  const pokemonNameSplitted = pok.name.split('-');

  return (
    <>
      <div className={`pokemon-card ${pokemonMainType}`} onClick={toggleModal}>
        <div className="img-container">
          <img src={pok.sprites.other.dream_world.front_default} alt="" />
        </div>
        <div className="info-container">
          <span className="id">#{pok.id}</span>
          <p className="name">{pokemonNameSplitted[0]}</p>
          <small className="type">Type: {pokemonMainType}</small>
        </div>
      </div>
      {showModal ? <PokemonModal pok={pok} func={toggleModal} /> : ""}
    </>
  );
}
