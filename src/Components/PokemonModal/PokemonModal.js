import React from "react";
import "./PokemonModal.scss";
import "./PokemonStats.scss";
import closeIcon from "../../Icons/closeIcon.svg";

export default function PokemonModal({ pok, func }) {
  const pokemonMainType = pok.types[0].type.name;
  return (
    <div className="overlay">
      <div className="modal">
        <div className="modal-content">
          <div className={`modal-header ${pokemonMainType}`}>
            <h2 className="pokemon-name">{pok.name} - #{pok.id}</h2>
            <button className="close-modal" onClick={func}>
              <img src={closeIcon} alt="" />
            </button>
          </div>
          <div className="modal-body">
            <div className="img-container">
              <img src={pok.sprites.other.dream_world.front_default} alt="" />
            </div>
            <div className="stats-container">
              <div className="types">
                {pok.types.map((type) => {
                  return (
                    <p className={`type ${type.type.name}`}>
                      {type.type.name}
                    </p>
                  );
                })}
              </div>
              <div className="fight-stats">
                <ul className="fight-list">
                  <li className="fight-element">
                    PV :{" "}
                    <div className="progress-bar">
                      <div
                        className={`progress-bar-data w-${pok.stats[0].base_stat}`}
                      >
                        {pok.stats[0].base_stat}
                      </div>
                    </div>
                  </li>
                  <li className="fight-element">
                    Attaque :{" "}
                    <div className="progress-bar">
                      <div
                        className={`progress-bar-data w-${pok.stats[1].base_stat}`}
                      >
                        {pok.stats[1].base_stat}
                      </div>
                    </div>
                  </li>
                  <li className="fight-element">
                    Défense :{" "}
                    <div className="progress-bar">
                      <div
                        className={`progress-bar-data w-${pok.stats[2].base_stat}`}
                      >
                        {pok.stats[2].base_stat}
                      </div>
                    </div>
                  </li>
                  <li className="fight-element">
                    Attaque Spéciale :{" "}
                    <div className="progress-bar">
                      <div
                        className={`progress-bar-data w-${pok.stats[3].base_stat}`}
                      >
                        {pok.stats[3].base_stat}
                      </div>
                    </div>
                  </li>
                  <li className="fight-element">
                    Défense Spéciale :{" "}
                    <div className="progress-bar">
                      <div
                        className={`progress-bar-data w-${pok.stats[4].base_stat}`}
                      >
                        {pok.stats[4].base_stat}
                      </div>
                    </div>
                  </li>
                  <li className="fight-element">
                    Vitesse :{" "}
                    <div className="progress-bar">
                      <div
                        className={`progress-bar-data w-${pok.stats[5].base_stat}`}
                      >
                        {pok.stats[5].base_stat}
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="phys-stats">
                <p>
                  <strong>Poids: </strong>
                  {pok.weight / 10} kg
                </p>
                <p>
                  <strong>Taille: </strong>
                  {pok.height / 10} m
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
